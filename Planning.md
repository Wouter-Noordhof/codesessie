# Git CodeSessie

## Programma

1. Algemene uitleg over Git commands.
2. CLI demo 👀
3. Git GUI's
4. Suite Seven Git workflow
5. Workflow demo
6. Afsluiting

## Git Commands

- [Basis Git Commands](https://wiki.suiteseven.nl/git-basis/)
- [Geavanceerde Git Commands](https://wiki.suiteseven.nl/git-geavanceerd/)

## It's CLI Demo time! `-awesome music plays-`

1. Repo clonen via SSH: `git clone git@gitlab.com:Wouter-Noordhof/codesessie.git`
2. De readme aanpassen.
3. De status bekijken van de repo `git status`
4. De readme stagen, committen en pushen. `git add README.md` | `git commit -m "Updated the README file"` | `git push -u origin master` 
5. Nu een branch aanmaken `git branch testbranch` 
6. Eerst nu op de master de readme aanpassen.
7. Op deze branch aanpassen `git checkout testbranch` 
8. Weer stagen, committen en pushen.

In GitKraken laten zien wat er is gebeurt.

Coole Git log: `git log --graph –oneline --decorate` 

## Git GUI's

De behandelde onderdelen even kort laten zien in SourceTree en evt. GitKraken.

## S7 Git Workflow

- [Workflow op wiki](https://wiki.suiteseven.nl/suite-seven-git-workflow/)

## Workflow Demo
 
1. Iedereen een eigen feature branch aanmaken vanuit de staging branch -> `feature-<je eigen naam>`.
2. Op je eigen branch allemaal de tweede regel van de readme aanpassen, stagen, committen en pushen naar je branch.
   (mag ook meer dan één ding aanpassen, als je maar ook de tweede regel aanpast)
3. Nu op GitLab een pull request aanmaken voor jouw eigen branch naar de master.

## Afsluiting

Vragen? 👻

Huiswerk: [Git branching leren](https://learngitbranching.js.org/)
